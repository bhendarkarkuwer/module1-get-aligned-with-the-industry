**CNN**
A Convolutional Neural Network (ConvNet/CNN) is a Deep Learning algorithm which can take in an input image, assign importance (learnable weights and biases) to various aspects/objects in the image and be able to differentiate one from the other

[CNN](https://drive.google.com/file/d/1neLtMIYsaVdQtPZTPpqeaYizliGtKt3W/view?usp=sharing)




- [1]  The whole process start by creating a filter which has different features like colour, size, shape and in case of bird and fish the feature can also be eyes, nose, foot and various other body parts.

- [2]  The whole process of applying the filter and convolution is done until we get a fine output which is good enough to classify the image.

- [3] Then comes the activation function which helps to decide if the neuron would fire or not ie the neuron is active or not. There are many types of activation function the most commonly used is Rectified Linear Unit (ReLU).

- [4] After this feature maps which are the output containing the features above mentioned. this output we get from the convolution . So the feature maps are flattened so that they come in single vector which then becomes the first layer of deep neural network which is also known as the fully connected layer.
- [5] At last we get an output layer which classifies the output and gives the probability of each class, for which normally softmax is used.

**Kernel size (K)**: How big your sliding windows are in pixels. Small is generally better and usually odd value such as 1,3,5 or sometimes rarely 7 are used.

**Stride (S)**: How many pixels the kernel window will slide at each step of convolution. This is usually set to 1, so no locations are missed in an image but can be higher if we want to reduce the input size down at the same time.

**Zero padding (pad)** : The amount of zeros to put on the image border. Using padding allows the kernel to completely filter every location of an input image, including the edges.

**Number of filters (F)**: How many filters our convolution layer will have. It controls the number of patterns or features that a convolution layer will look for.

**Flattening** : Flattening layer is used to bring all feature maps matrices into a single vector
