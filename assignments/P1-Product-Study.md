# P1 - Study industry ready products

## Product 1

**Product Name**

Surface Scan System

**Product link**

[link](https://www.argutec.eu/surface-scan-system)

**Product Short description**

Contactless monitoring system for defect evaluation on a grain oriented electrical steel strip. System is flexible and can be used in different industrial areas and with assortment of other materials – plastic, textile, stainless steel, pharmaceutical, food industry, 

**Product is combination of features**

- Object detection
- ID Detection


**Product is provided by which company?**

Argutec


## Product 2

**Product Name**

Image Tagging

**Product link**

[link](https://www.clarifai.com/)

**Product Short description**

Clarifai is an image recognition platform that helps users organize, curate, filter and search their media.

**Product is combination of features**

- Object detection
- Segmentation

**Product is provided by which company?**

CLARIFAI

## Product 3

**Product Name**

Facial Recognition

**Product link**

[link](https://facex.io/)

**Product Short description**

The company’s technology employs state-of-the-art tracking technology to detect people even under low illumination and varied poses.

**Product is combination of features**

- Face recognition
- Face Detection


**Product is provided by which company?**

FaceX

## Product 4

**Product Name**

Object detection

**Product link**

[link](http://www.restb.ai/)

**Product Short description**

Restb.ai offers a cloud Deep Learning Image Recognition API. By using Artificial Intelligence technology, images and videos can be understood, moderated, filtered, categorized, classified or tagged in almost real time obtaining very high confidence scores.

**Product is combination of features**

- Object detection
- Segmentation


**Product is provided by which company?**

restb










